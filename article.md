---
title: "Za pití v centru můžou večerky, tvrdí pražští strážníci"
perex: "Interaktivní mapa míst, kde se v Praze žebrá, pije alkohol či nabízí erotické služby."
description: "Nonstop večerky se snadno dostupným alkoholem v každé ulici. To je je podle strážníků důvod, proč se nedaří v centru metropole omezit pití na veřejnosti."
authors: ["Jan Cibulka", "Jan Charvát", "Marcel Šulek"]
published: "2. června 2016"
url: "mpp-poradek"
socialimg: http://www.mppraha.cz/images/phocagallery/autohlidka_praha9_fotoserial/thumbs/phoca_thumb_l__94a5096.jpg
libraries: [jquery, highcharts]
---

*„Vždyť to mám v tašce,“* vymlouvá se podnapilý muž policistům, kteří k němu vyjeli na základě oznámení z kamerového systému. Hlídku během dne doprovázel i redaktor Českého rozhlasu, který zjišťoval, jak vypadá práce městské policie v terénu.

Policisté pijanovi vysvětlují, že je jedno, jak má alkohol zabalený, ale v centru vyhláška zakazuje pití na ulici úplně. Ten se ještě chabě brání, že má to víno *„namíchané s kolou,“* ale nakonec si nechá vypsat pokutový bloček. *„Tím, že jste se dopouštěl protiprávního jednání, tak tady po sobě uklidíte, vezmete si věci a půjdete pryč,“* zakončuje jeden z hlídkujících strážníků, muž si sbírá tašky a odchází.

Strážnici přiznávají, že k placení pokut se pachatelé většinou nemají, zvlášť když jde o osoby bez domova. Zásah podle nich ale smysl má. *„Ten člověk už ví, že ho sledují kamery,“* vysvětlují a doufají, že teď si víno dopije někde v ústraní, a ne na veřejnosti, což je účel vyhlášky. *„Pokud  ho chytíme opakovaně, jde o recidivu a správní orgán mu může uložit zákaz pobytu v oblasti, kde se dopouštěl toho protiprávního jednání,“* dodávají policisté. Pokud by se dotyčný zákazem pobytu neřídil, hrozil by mu postih za maření úředního rozhodnutí, což už je trestný čin.

*Vybrané přestupky proti veřejnému pořádku můžete prozkoumat v interaktivní mapě.*

<aside class="big">
  <iframe src="https://samizdat.cz/data/mpp-prestupky-2015-bezpecnost/www/" class="ig" width="1024" height="710" scrolling="no" style="margin: 10px 0px;" frameborder="0"></iframe>
</aside>

### Doprava hraje prim

*„Městská policie by nemusela mít pravomoc měření rychlosti, primárně by měla sloužit občanům města, řešit veřejný pořádek,“* řekl v polovině května ministr vnitra Milan Chovanec deníku Právo k poslanecké iniciativě, která chce okleštit pravomoc strážníků měřit rychlost.

Pražská městská policie ročně eviduje skoro 700 tisíc přestupků, 292 tisíc případů zachytí systém automatického měření rychlosti, o kterém jsme [nedávno psali](http://www.rozhlas.cz/zpravy/data/_zprava/interaktivni-mapa-kde-si-dat-pozor-na-automaticke-mereni-rychlosti-v-praze--1614412). Ze zbývajících 390 tisíc pak většinu tvoří špatné parkování a další dopravní agendy, jen 11 tisíc jsou přestupky proti veřejnému pořádku.

<aside class="small">
  <div id="container" style="height: 200px"></div>
</aside>

Neexistuje žádný přesný seznam toho, co se mezi veřejný pořádek počítá a co ne, Český rozhlas se proto v analýze zaměřil na vybrané přestupky, které jsou v Praze nejčastější a zároveň nesouvisí s dopravou. Nutno dodat, že následující mapy nejsou úplné, vůbec se nezaznamenávají blokové pokuty do výše tisíc korun, uhrazené na místě, případně přestupky vyřešené domluvou.

Z evidovaných přestupků proti veřejnému pořádku vede právě porušení zákazu konzumace alkoholu, za loňský rok zaznamenali strážníci 1950 případů. Jde zejména o centrum města, konkrétně třeba o Vrchlického sady před hlavním nádražím, Václavské náměstí či ulici Na příkopě. Zákaz se také příliš nedodržuje mezi Andělem a Smíchovem a pak v Libni.

### Kamery jako důkaz

*„Už jich není tolik, kolik jich bývalo. Většinou nám to předává kamerový pracovník, který je natočí. Oni před nám logicky nepijí,“* vysvětluje jeden ze strážníků, které v centru hlídkuje. Dodává, že velká část práce stojí právě na kamerovém systému. Správní orgány, které následně přestupky řeší, chtějí mít vše neprůstřelně zdokumentované.

Kde je pití zakázáno, stanovuje pražská [vyhláška č. 12/2008 Sb.](http://www.praha.eu/public/8e/3b/ad/1600089_389763_alkohol.pdf), magistrát pak zpracoval mapku všech dotčených míst. Obecně je alkohol zakázaný u zastávek MHD, na nádražích, poblíž dětských hřišť, v pracích a u obchodních center. Do vyhlášky se pak obvykle dostanou i místa, která městské části označí jako riziková. Nejvíc je jich na Praze 8, 4 a 1.

<aside class="big">
  <iframe src="https://mapsengine.google.com/map/view?mid=z0mzBk4iAJX0.kcvG8SkfXCuY" width="1024" height="600"></iframe>
</aside>

„Problém je dostupnost alkoholu jako takového,“ shodují se hlídkující strážníci a ukazují na všudypřítomné večerky s lihovinami. Například ve Štěpánské ulici napočítají hned čtyři. Takové podniky prý raketovou rychlostí přibývají, v centru města je totiž výnosný obchod. A zatím co pití z restaurací si lidé těžko budou nosit do ulic, u malých krámků to neplatí. Kdyby byl prodej alkoholu v centru regulovaný, drtivá většina souvisejících přestupků by prakticky vymizela, myslí si policisté.

A kdo zákaz pití překračuje nejčastěji? „V prvním případě bezdomovci, většinou přes den, a turisté, kteří vyhlášku neznají. V noci jsou to nejen turisté, ale i Češi, kteří o zákazu vědí, ale i tak vyhlášku porušují,“ vysvětlil rozhlasu strážník Petr Spěvák z automobilové hlídky městské policie. Protiprávní jednání někdy nahlašují občané, častěji ho ale zachytí kamerový systém. *„Pracovník na kamerovém systému se sám snaží vyhledávat místa, kde k tomu jednání dochází,“* popisují strážníci s tím, že například prostor před hlavním nádražím je sledovaný nepřetržitě a vždy je nějaká hlídka v blízkosti.

*Případy pití na veřejnosti*

<aside class="big">
  <iframe src="https://samizdat.cz/data/mpp-prestupky-2015-bezpecnost/www/#2" class="ig" width="1024" height="710" scrolling="no" style="margin: 10px 0px;" frameborder="0"></iframe>
</aside>

V počtu evidovaných prohřešků proti veřejnému pořádku je na druhém místě porušení zákazu žebrání. I tuto činnost upravuje městská vyhláška, konkrétně [14/2000 Sb.](http://www.praha.eu/jnp/cz/o_meste/vyhlasky_a_narizeni/vyhledavani_v_pravnich_predpisech/rok_2000-vyhlaska_cislo_14_ze_dne_27_04_2000.html), které kromě konkrétního seznamu zapovězených míst obsahuje i obecný zákaz žebrání v okolí zastávek veřejné dopravy či u úřadů a škol.

V Praze se žebráci soustředí na takzvané Královské cestě, tedy v okolí Staroměstského náměstí, na Karlově mostě a podél cesty na hrad. Občas tohle jednání strážníci pokutují i v okolí náměstí Míru a na Smíchově.

### Lustrace a vykázání

Právě k žebrákovi poblíž Karlovu most hlídku zavolal operátor městských kamer. *„Vy jste známá firma, takže víte, že se tu žebrat nemá,“* oznamují strážníci žebrákovi, který před jejich příjezdem ležel s nataženýma rukama za jedním z odpadkových košů. Ten říká, že nedávno dorazil z Ostravy a chybí mu peníze na jídlo. Policisté hned oponují, že jídlo osobám bez domova poskytují charitativní organizace, žebráci si tak za získané peníze spíše kupují alkohol.  Muže lustrují v databázi hledaných osob a následně ho upozorňují, že pokud bude v porušování vyhlášky pokračovat, kromě pokut ho radnice z centra vykáže.

Podle strážníka Jiřího Pařízka, který u městské policie v Praze slouží už 23 let, sice pachatelé pokuty často neplatí, ale právě vykázání je pro ně citelný trest. Přiznává, že nakonec se člověku mohou vymstít i nahromaděné pokutové bločky. *„Ve chvíli, kdy člověk získá nějaké zaměstnání, tak ho ty exekuce doženou,“* vysvětluje s tím, že dokonce zná drobné pachatele, kteří mají kvůli porušování veřejného pořádku na radnici splátkový kalendář.

Hned ale dodává, že žebrání v centru se daří omezovat. *„Díky vyhlášce tohle narušování veřejného pořádku výrazně ubylo,“* a dodává, že v minulosti v centru operovaly i rumunské žebrácké gangy. Dnes jde obvykle o lidi bez prostředků, strážníci je často znají i jménem.

*Žebrání v rozporu s vyhláškou*

<aside class="big">
  <iframe src="https://samizdat.cz/data/mpp-prestupky-2015-bezpecnost/www/#3" class="ig" width="1024" height="710" scrolling="no" style="margin: 10px 0px;" frameborder="0"></iframe>
</aside>

Podle policejní evidence si jak pijící, tak i žebrající přestupci nejčastěji převezmou pokutový bloček, není ale jasné, jaká část se podaří opravdu vybrat. *„Statistiku úspěšnosti výběru blokových pokut na místě nezaplacených nevedeme,“* napsala Rozhlasu šéfka kanceláře ředitele městské policie Irena Seifertová. Pokud by se dařilo vybrat vše, město by si na pokutách za přestupky proti veřejnému pořádku ročně přišlo na 22 milionů korun. Jde tak jen zlomek peněz, příjmy městské kasy na rychlostních pastech Český rozhlas [odhadl na 197 milionů korun](http://www.rozhlas.cz/zpravy/data/_zprava/interaktivni-mapa-kde-si-dat-pozor-na-automaticke-mereni-rychlosti-v-praze--1614412).

Ne za každým výjezdem je ale vědomé porušování městských vyhlášek. Během středeční služby strážníci vyjížděli i k muži krmícímu holuby. *“Když je jasné, že to ti holubi zjevně nemohou sníst, tak je to znečišťování veřejného prostranství,“* vysvětlují reportérovi Rozhlasu. Na místě pak se starším pánem řeší, že s velkým kusem rohlíku si ptactvo neporadí. *„Pokud je chcete krmit, tak jim házejte malé drobečky.“*
