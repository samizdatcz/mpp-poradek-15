$(function () {
	    $('#container').highcharts({
	        chart: {
	            type: 'bar'
	        },
	        credits: {
	        	enabled: false
	        },
	        title: {
	            text: ''
	        },
	        colors: [
	        	'rgb(166,206,227)',
	        	'rgb(31,120,180)',
	        	'rgb(227,26,28)'
	        	],
	        xAxis: {
	            categories: ['Přestupky']
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'počty přestupků v roce 2015'
	            }
	        },
	        legend: {
	            reversed: true
	        },
	        plotOptions: {
	            series: {
	                stacking: 'normal'
	            }
	        },
	        series: [{
	            name: 'Automatické měření rychlosti',
	            data: [292117]
	        }, {
	            name: 'Ostatní doprava (zejména parkování)',
	            data: [380144]
	        }, {
	            name: 'Veřejný pořádek',
	            data: [10931]
	        }]
	    });
	});